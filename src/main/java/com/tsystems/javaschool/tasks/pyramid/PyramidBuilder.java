package com.tsystems.javaschool.tasks.pyramid;



import java.util.Comparator;
import java.util.List;

public class PyramidBuilder {

    /**
     * Builds a pyramid with sorted values (with minumum value at the top line and maximum at the bottom,
     * from left to right). All vacant positions in the array are zeros.
     *
     * @param inputNumbers to be used in the pyramid
     * @return 2d array with pyramid inside
     * @throws {@link CannotBuildPyramidException} if the pyramid cannot be build with given input
     */
    public int[][] buildPyramid(List<Integer> inputNumbers) {

        if (inputNumbers == null || inputNumbers.size() == 0 || inputNumbers.contains(null)) {
            throw new CannotBuildPyramidException();
        }
        //find width and height of pyramid
        int pyramidHeight = getHeight(inputNumbers.size());
        int pyramidWidth = pyramidHeight * 2 - 1;
        int[][] result = new int[pyramidHeight][];

        //sort the array
        inputNumbers.sort(Comparator.naturalOrder());

        //insertion into the pyramid
        int indexInList = 0;
        for (int i = 0; i < pyramidHeight; i++) {
            result[i] = new int[pyramidWidth];
            //how many elements skip in the row for well-formating
            int skip = pyramidHeight - i - 1;
            for (int j = skip; j < pyramidWidth - skip; j++) {

                //insert an list's element and skip 1 element in pyramid
                result[i][j++] = inputNumbers.get(indexInList++);
            }
        }
        return result;
    }

    /**
     * Count height of pyramid
     *
     * @param size List size
     * @return pyramid height
     * @throws {@link CannotBuildPyramidException} height cannot be count
     */
    private int getHeight(int size) throws CannotBuildPyramidException {
        int height= 0;
        int count = 0;
        while (size > count && count>=0) {
            count += ++height;
        }
        if (size != count)
            throw new CannotBuildPyramidException();
        return height;
    }

}
