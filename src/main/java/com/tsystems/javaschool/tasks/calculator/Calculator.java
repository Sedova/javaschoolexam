package com.tsystems.javaschool.tasks.calculator;


import java.util.*;


public class Calculator {


    /**
     * Evaluate statement represented as string.
     *
     * @param statement mathematical statement containing digits, '.' (dot) as decimal mark,
     *                  parentheses, operations signs '+', '-', '*', '/'<br>
     *                  Example: <code>(1 + 38) * 4.5 - 1 / 2.</code>
     * @return string value containing result of evaluation or null if statement is invalid
     */
    public String evaluate(String statement) {

        if (statement == null || statement.isEmpty())
            return null;

        String result;
        try {
            // Transform expression into postfix notation
            // e.g. ""2+3*4" will be transformed to ["2", "3","4", "*", "+" ]
            String[] postfix = infixToPostfix(statement);

            // Evaluate an expression
            result = PostfixToResult(postfix);

        } catch (RuntimeException exception){
            //there is errors while evaluating
              result = null;
        }
        return result;
    }

    // Operators
    private static final Map<String, Integer> OPERATORS = new HashMap<>();

    static {
        // Map<operator,priority>
        OPERATORS.put("+", 0);
        OPERATORS.put("-", 0);
        OPERATORS.put("*", 1);
        OPERATORS.put("/", 1);
    }


    /**
     * Check if token is in operators
     *
     * @param token token to check
     * @return true if token is a operator
     */
    private static boolean isOperator(String token) {
        return OPERATORS.containsKey(token);
    }

    /**
     * Return relative priority of operations.(compare token1's priority to token2's priority)
     *
     * @param token1 token to check
     * @param token2 another token
     * @return positive if token1's priority is higher, negative if token2' pririty is higher,
     * otherwise 0
     */
    private static final int relPriority(String token1, String token2) {
        if (!isOperator(token1) || !isOperator(token2)) {
            throw new IllegalArgumentException();
        }
        return OPERATORS.get(token1) - OPERATORS.get(token2);
    }

    /**
     * Convert infix notation format into postfix notation
     *
     * @param expression expression to convert
     * @return List of tokens in postfix notation
     */
    private static String[] infixToPostfix(String expression){
        // Open parentheses count
        short parenthesesOpen = 0;
        ArrayList<String> out = new ArrayList<>();
        Stack<String> stack = new Stack<>();


        for (int i = 0; i < expression.length(); i++) {
            String token = String.valueOf(expression.charAt(i));
            // If token is an operator
            if (isOperator(String.valueOf(token))) {
                // While stack not empty AND stack top element
                // is an operator
                while (!stack.empty() && isOperator(stack.peek())) {

                    if (relPriority(token, stack.peek()) <= 0) {
                        out.add(stack.pop());
                    }
                    else {
                        break;
                    }
                }
                // Push the new operator on the stack
                stack.push(token);
            }
            // If token is a left parentheses
            else if (token.equals("(")) {
                parenthesesOpen++;
                stack.push(token);  //
            }
            // If token is a right parentheses ')'
            else if (token.equals(")")) {
                while (!stack.empty() && !stack.peek().equals("(")) {
                    out.add(stack.pop());
                }
                stack.pop();
                parenthesesOpen--;
            }
            // If token is a number or '.'
            else {
                StringBuilder sb = new StringBuilder();
                // Loop while j<expression.length and charAt(j) is digit or comma
                for (int j = i; j < expression.length(); ++j) {
                    if (Character.isDigit(expression.charAt(j)) || expression.charAt(j) == '.') {
                        sb.append(expression.charAt(j));
                        i++;
                    } else {

                        break;
                    }
                }
                // If string is empty - expression contains forbidden charachers
                if (sb.length()==0)
                {
                    throw new IllegalArgumentException();
                }
                // Else add string into stack and decrement counter i
                else
                {
                    out.add(sb.toString());
                    i--;
                }
            }
        }
        // If there is open parentheses throw an IllegalArgumentException;
        if (parenthesesOpen > 0)
            throw new IllegalArgumentException();
        while (!stack.empty()) {
            out.add(stack.pop());
        }
        String[] output = new String[out.size()];
        return out.toArray(output);
    }
    /**
     * Evaluate expression in postfix notation
     *
     * @param tokens List of tokens
     * @return String result if expression has been evaluated
     */
    private static String PostfixToResult(String[] tokens) {
        Stack<String> stack = new Stack<>();

        // For each token
        for (String token : tokens) {
            // If the token is a number push it into the stack
            if (!isOperator(token)) {
                stack.push(token);
            } else {
                // Token is an operator: pop 2 tokens
                double d2 = Double.valueOf(stack.pop());
                double d1 = Double.valueOf(stack.pop());

                //Get the result
                double result;

                switch (token) {
                    case "+":
                        result = d1 + d2;
                        break;
                    case "-":
                        result = d1 - d2;
                        break;
                    case "*":
                        result = d1 * d2;
                        break;
                    case "/":
                        if (d2 == 0)
                            throw new ArithmeticException("Division by zero");
                        result = d1 / d2;
                        break;
                    default:
                        throw new UnsupportedOperationException();
                }
                // Push result into stack
                stack.push(String.valueOf(result));
            }
        }

        // Format the result
        double result = Double.valueOf(stack.pop());

        if (result == (int) result)
            // return an integer as string
            return String.valueOf((int) result);
        else
            // return double
            return String.valueOf(result);
    }

}

