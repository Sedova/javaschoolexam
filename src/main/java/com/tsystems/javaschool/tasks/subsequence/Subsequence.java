package com.tsystems.javaschool.tasks.subsequence;

import java.util.List;

public class Subsequence {

    /**
     * Checks if it is possible to get a sequence which is equal to the first
     * one by removing some elements from the second one.
     *
     * @param x first sequence
     * @param y second sequence
     * @return <code>true</code> if possible, otherwise <code>false</code>
     */
    @SuppressWarnings("rawtypes")
    public boolean find(List x, List y) {

        if (x == null || y == null)
            throw new IllegalArgumentException();
        if (x.isEmpty())
            return true;

        boolean result = false;
        // Index for element to find from list X
        int xIndex = 0;
        // Iterate through y list
        for (Object aY : y) {

            // If element of X is in Y list - increment X index
            if (aY.equals(x.get(xIndex)))
                xIndex++;
            // If all elements are in Y List  - no need to continue iterate Y
            if (xIndex == x.size() - 1) {
                result = true;
                break;
            }
        }
        return result;
    }
}
